import { graphql } from 'gatsby';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Layout from '../components/layout';

class PageTemplate extends Component {
    render() {
        const post = this.props.data.wordpressPage;

        // @TODO: STEP #5: Use title and content in Gatsby.
        return (
            <Layout>
                <h1 dangerouslySetInnerHTML={{ __html: post.title }} />
                <div dangerouslySetInnerHTML={{ __html: post.content }} />
            </Layout>
        );
    }
}

PageTemplate.propTypes = {
    data: PropTypes.object.isRequired,
    edges: PropTypes.array
};

export default PageTemplate;

// @TODO: STEP #4: Get current WP Post data via ID.
export const pageQuery = graphql`
    query($id: String!) {
        wordpressPage(id: { eq: $id }) {
            title
            content
        }
    }
`;
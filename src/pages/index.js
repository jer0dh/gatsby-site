import React from "react"
import { Link, graphql } from "gatsby"
import Img from "gatsby-image"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import { sanitize } from 'dompurify'


const IndexPage = ({data}) => (
  <Layout>
    <SEO title="Home" />
    <article className="page entry" id="portfolio">
      <header>
        <div className="wrap">
          <h2 className="entry-title" itemprop="headline">Portfolio</h2>
        </div>
      </header>
      <section className="banner section-portfolio-feature">
        <div className="wrap">
          {data.allWordpressWpPortfolio.edges.map(({ node }) => (
            
            <div className="portfolio-feature" data-id="{node.wordpress_id}">
              <div className="row">
                <div className="portfolio-feature-feature col-md-4 order-0">
                  <Img fluid={node.featured_media.localFile.childImageSharp.fluid} alt="{node.featured_media.alt_text}" />
                </div>
                <div className="portfolio-feature-desc col-md-5 order-3 order-md-1">
                  <h3>{node.title}</h3>
                  <div>
                    <div dangerouslySetInnerHTML={{ __html: sanitize(node.content) }}></div>
                  </div>
                </div>
                <div className="portfolio-feature-meta col-md-3 order-1 order-md-2 mb-4">
                  <div>
                    <div>
                        <strong>Type:</strong> { dump( node.portfolio_categories[0], 'none' ) } 
                  </div>
                    <div>
                      <strong>URL:</strong>
                      <a href="{sanitize(node.acf.external_url)}">{sanitize(node.acf.external_url)}</a>
                    </div>
                    <div>
                      <strong>Company:</strong> {sanitize( node.acf.company ) }
                  </div>
                  </div>
                </div>

              </div>
	                
            </div>
          ))}

        </div>
      </section>
    </article>
   
    <ul> 
      {data.allWordpressWpPortfolio.edges.map( ({ node }) => (
        <li> {node.title} </li>
      )) }
      
    </ul>

  </Layout>
)

/* repeatString() returns a string which has been repeated a set number of times */
function repeatString(str, num) {
    let out = '';
    for (var i = 0; i < num; i++) {
        out += str;
    }
    return out;
}

/*
dump() displays the contents of a variable like var_dump() does in PHP. dump() is
better than typeof, because it can distinguish between array, null and object.
Parameters:
    v:              The variable
    howDisplay:     "none", "body", "alert" (default)
    recursionLevel: Number of times the function has recursed when entering nested
                    objects or arrays. Each level of recursion adds extra space to the
                    output to indicate level. Set to 0 by default.
Return Value:
    A string of the variable's contents
Limitations:
    Can't pass an undefined variable to dump(). 
    dump() can't distinguish between int and float.
    dump() can't tell the original variable type of a member variable of an object.
    These limitations can't be fixed because these are *features* of JS. However, dump()
*/
function dump(v, howDisplay, recursionLevel) {
    howDisplay = (typeof howDisplay === 'undefined') ? "alert" : howDisplay;
    recursionLevel = (typeof recursionLevel !== 'number') ? 0 : recursionLevel;

    var vType = typeof v;
    var out = vType;

    switch (vType) {
        case "number":
        /* there is absolutely no way in JS to distinguish 2 from 2.0
           so 'number' is the best that you can do. The following doesn't work:
           var er = /^[0-9]+$/;
           if (!isNaN(v) && v % 1 === 0 && er.test(3.0)) {
               out = 'int';
           }
        */
        break;
    case "boolean":
        out += ": " + v;
        break;
    case "string":
        out += "(" + v.length + '): "' + v + '"';
        break;
    case "object":
        //check if null
        if (v === null) {
            out = "null";
        }
        //If using jQuery: if ($.isArray(v))
        //If using IE: if (isArray(v))
        //this should work for all browsers according to the ECMAScript standard:
        else if (Object.prototype.toString.call(v) === '[object Array]') {
            out = 'array(' + v.length + '): {\n';
            for (var i = 0; i < v.length; i++) {
                out += repeatString('   ', recursionLevel) + "   [" + i + "]:  " +
                    dump(v[i], "none", recursionLevel + 1) + "\n";
            }
            out += repeatString('   ', recursionLevel) + "}";
        }
        else {
            //if object
            let sContents = "{\n";
            let cnt = 0;
            for (var member in v) {
                //No way to know the original data type of member, since JS
                //always converts it to a string and no other way to parse objects.
                sContents += repeatString('   ', recursionLevel) + "   " + member +
                    ":  " + dump(v[member], "none", recursionLevel + 1) + "\n";
                cnt++;
            }
            sContents += repeatString('   ', recursionLevel) + "}";
            out += "(" + cnt + "): " + sContents;
        }
        break;
    default:
        out = v;
        break;
    }

    if (howDisplay == 'body') {
        var pre = document.createElement('pre');
        pre.innerHTML = out;
        document.body.appendChild(pre);
    }
    else if (howDisplay == 'alert') {
        alert(out);
    }

    return out;
}

export const query = graphql`
query myQuery {
  allWordpressPost(filter: {categories: {elemMatch: {slug: {eq: "featured"}}}}) {
    edges {
      node {
        title
        slug
        excerpt
        date
        featured_media {
          localFile {
            childImageSharp {
              fluid(maxWidth: 700) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  }
  allWordpressWpPortfolio(filter: {status: {eq: "publish"}}) {
    edges {
      node {
        acf {
          company
          date_published
          external_url
          screenshots {
            localFile {
              childImageSharp {
                fluid(maxWidth: 700) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
        content
        date
        slug
        title
        wordpress_id
        portfolio_categories {
          name
        }
        featured_media {
          alt_text
          localFile {
            childImageSharp {
              fluid(maxWidth: 700) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  }
}
`
export default IndexPage

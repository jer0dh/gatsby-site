import React from "react"
import { Link, StaticQuery, graphql } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"


const ContactPage = () => (
  <StaticQuery
    query={graphql`
      query HeadingQuery {
        site {
          siteMetadata {
            title
          }
        }
        allWordpressPost {
            edges {
                node {
                    id
                    slug
                    status
                    template
                    format
                }
            }
        }
      }

    `}
    render={data => (
      <Layout>
        <SEO title="Contact" />
        <h1>Contact Me</h1>
        <p>This is the contact pagek</p>
        <ul>
          {data.allWordpressPost.edges.map(({ node }, i) => (
            <li key={i}> {node.slug}</li>
          ))}
        </ul>
      </Layout>
    )}
  />
)
export default ContactPage
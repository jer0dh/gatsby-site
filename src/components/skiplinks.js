
import PropTypes from "prop-types"
import React from "react"

const SkipLinks = ({ siteTitle }) => (
    <ul className="skip-link">
        <li>
            <a href="#nav-primary" className="sronly"> Skip to primary navigation</a>
        </li>
        <li>
            <a href="#maincontent" className="sronly"> Skip to content</a>
        </li>
        <li>
            <a href="#footer" className="sronly"> Skip to footer</a>
        </li>
    </ul>
)

// NOTE: using Link with hashtags only caused the link to go to the root of the site.
// TODO: change prop to array.  Have the three as default.. have capability to send array prop with additional links

SkipLinks.propTypes = {
  siteTitle: PropTypes.string,
}

SkipLinks.defaultProps = {
  siteTitle: ``,
}

export default SkipLinks


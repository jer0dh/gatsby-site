import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styles from "./footerwidgets.module.scss"

function FooterWidget() {
    return (
        <div className={`footer-widgets ${styles.footer}`} id="genesis-footer-widgets">
            <h2 className="genesis-sidebar-title screen-reader-text">Footer</h2>
            <div className="container">
                <div className="row">
                    <div className="widget-area footer-widgets-1 footer-widget-area">
                        <section id="text-2" className="widget widget_text">
                            <div className="widget-wrap">			
                                <div className="textwidget">
                                    <a href="https://github.com/jer0dh?tab=repositories">
                                        <img className="aligncenter size-full wp-image-1620" src="https://i2.wp.com/jhtechservices.com/wp-content/uploads/github.png?resize=120%2C120&amp;ssl=1" alt="" width="120" height="120" />
                                    </a>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div className="widget-area footer-widgets-2 footer-widget-area">
                        <section id="nav_menu-3" className="widget widget_nav_menu">
                            <div className="widget-wrap">
                                <div className="menu-main-container">
                                    <ul id="menu-main-1" className="menu">
                                        <li className="menu-item menu-item-type-post_type menu-item-object-page menu-item-1553"><a href="#portfolio" itemprop="url">Portfolio</a></li>
                                        <li className="menu-item menu-item-type-post_type menu-item-object-page menu-item-345"><a href="#knowledgebase-tips-from-the-field" itemprop="url">Knowledgebase</a></li>
                                        <li className="menu-item menu-item-type-post_type menu-item-object-page menu-item-916"><a href="#about" itemprop="url">About</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div className="widget-area footer-widgets-3 footer-widget-area">
                        <section id="text-7" className="widget widget_text">
                            <div className="widget-wrap">
                                <div className="textwidget">
                                    <p>
                                        <a href="https://jhtechservices.com">
                                            <img className="aligncenter size-full wp-image-1138" src="https://i2.wp.com/jhtechservices.com/wp-content/uploads/logo512.png?resize=644%2C512&amp;ssl=1" alt="" width="295" height="235" />
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FooterWidget
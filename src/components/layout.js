/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Helmet from 'react-helmet'

import Header from "./header"
import FooterWidgets from "./footerwidgets"
import "../css/style.scss"
import SkipLinks from "./skiplinks"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
      <Helmet>
        <body className="single wp-custom-logo header-image" />
      </Helmet>
      <div className="site-container">
        <SkipLinks></SkipLinks>
        <Header siteTitle={data.site.siteMetadata.title} />
        <div className="site-inner">
          <div className="content-sidebar-wrap">
            <main className="content" id="maincontent">
              {children}
            </main>
          </div>
          <FooterWidgets />
          <footer id="footer">
            © {new Date().getFullYear()}, Built with
            {` `}
            <a href="https://www.gatsbyjs.org">Gatsby</a>
          </footer>
        </div>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

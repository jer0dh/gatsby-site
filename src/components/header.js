import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styles from "./header.module.scss"

const Header = ({ siteTitle }) => (

  <header className={`site-header ${styles.header}`} itemscope="" itemtype="https://schema.org/WPHeader">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className="title-area">
            <h1 className="site-title" itemprop="headline">
              <Link to="/">JH Tech Services</Link>
            </h1>
            <p className="site-description" itemprop="description">A knowledgebase</p>
          </div>
          <button className="menu-toggle dashicons-before dashicons-menu" aria-expanded="false" aria-pressed="false" aria-label="Main Menu Button" id="genesis-mobile-nav-primary"></button>
          <nav className="nav-primary genesis-responsive-menu" aria-label="Main" itemscope="" itemtype="https://schema.org/SiteNavigationElement" id="genesis-nav-primary" >
            <ul id="menu-main" className="menu genesis-nav-menu menu-primary js-superfish sf-js-enabled sf-arrows" style={{ touchAction: 'pan-y;' }}>
              <li id="menu-item-1553" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-1553">
                <a href="#portfolio" itemprop="url">
                  <span itemprop="name">Portfolio</span>
                </a>
              </li>
              <li id="menu-item-345" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-345">
                <a href="#knowledgebase-tips-from-the-field" itemprop="url">
                  <span itemprop="name">Knowledgebase</span>
                </a>
              </li>
              <li id="menu-item-916" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-916">
                <Link to="contact" itemprop="url">
                  <span itemprop="name">Contact</span>
                </Link>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
